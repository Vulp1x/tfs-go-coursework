-- create user fintechuser with password 'admin';
--
-- create database fintech ;

grant all privileges on database fintech to fintechuser;

create table if not exists users
(
    id         serial not null
        constraint users_pkey
            primary key,
    last_name  text,
    first_name text,
    birthday   date,
    email      text
        constraint users_email_key
            unique,
    password   text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);

alter table users
    owner to fintechuser;



create table if not exists sessions
(
    session_id  serial not null
        constraint sessions_pkey
            primary key,
    user_id     integer,
    created_at  timestamp with time zone,
    valid_until timestamp with time zone
);

alter table sessions
    owner to fintechuser;

create table if not exists robots_with_deleted
(
    robot_id        serial      not null
        constraint table_name_pk
            primary key,
    owner_user_id   integer     not null,
    parent_robot_id integer     not null,
    is_favorite     boolean,
    ticker          varchar(50) not null,
    buy_price       double precision,
    sell_price      double precision,
    plan_start      timestamp with time zone,
    plan_end        timestamp with time zone,
    plan_yield      double precision,
    fact_yield      double precision,
    deals_count     integer,
    activated_at    timestamp with time zone,
    deactivated_at  timestamp with time zone,
    created_at      timestamp with time zone,
    deleted_at      timestamp with time zone,
    is_active       boolean,
    is_selling      boolean default false

);

alter table robots_with_deleted
    owner to fintechuser;

create index deleted_at_idx
    on robots_with_deleted (deleted_at);

create view robots
            (robot_id, owner_user_id, parent_robot_id, is_favorite, ticker, buy_price, sell_price, plan_start, plan_end,
             plan_yield, fact_yield, deals_count, activated_at, deactivated_at, created_at, deleted_at, is_active,
                is_selling)
as
SELECT robots_with_deleted.robot_id,
       robots_with_deleted.owner_user_id,
       robots_with_deleted.parent_robot_id,
       robots_with_deleted.is_favorite,
       robots_with_deleted.ticker,
       robots_with_deleted.buy_price,
       robots_with_deleted.sell_price,
       robots_with_deleted.plan_start,
       robots_with_deleted.plan_end,
       robots_with_deleted.plan_yield,
       robots_with_deleted.fact_yield,
       robots_with_deleted.deals_count,
       robots_with_deleted.activated_at,
       robots_with_deleted.deactivated_at,
       robots_with_deleted.created_at,
       robots_with_deleted.deleted_at,
       robots_with_deleted.is_active,
       robots_with_deleted.is_selling
FROM robots_with_deleted
WHERE robots_with_deleted.deleted_at IS NULL;

alter table robots
    owner to fintechuser;

