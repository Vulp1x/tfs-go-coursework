package nulltypes

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
)

type Time sql.NullTime

func (t *Time) UnmarshalJSON(body []byte) error {
	if body != nil {
		t.Valid = true
	}

	return json.Unmarshal(body, &t.Time)
}

func (t *Time) MarshalJSON() ([]byte, error) {
	if !t.Valid {
		return []byte(`""`), nil
	}

	return json.Marshal(t.Time)
}

func (t *Time) Scan(value interface{}) error {
	nullTime := sql.NullTime(*t)
	if err := nullTime.Scan(value); err != nil {
		return err
	}

	*t = Time(nullTime)

	return nil
}

func (t Time) Value() (driver.Value, error) {
	nt := sql.NullTime(t)

	return nt.Value()
}
