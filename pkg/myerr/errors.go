package myerr

var _ ErrorInterface = &WithMessage{}

type ErrorInterface interface {
	Error() string
	Cause() error
	Unwrap() error
	Is(target error) bool
}

type WithMessage struct {
	msg   string
	cause error
}

func (e WithMessage) Unwrap() error {
	return e.cause
}

func (e WithMessage) Cause() error {
	return e.cause
}

func (e WithMessage) Error() string {
	if e.cause == nil {
		return e.msg
	}

	return e.msg + ": " + e.cause.Error()
}

func (e *WithMessage) Is(target error) bool {
	return e.msg == target.Error()
}

func (e WithMessage) GetMessage() string {
	return e.msg
}

func WithMessages(err error, messages ...string) error {
	if messages == nil {
		return err
	}

	if err == nil {
		err = New(messages[0])
		messages = messages[1:]
	}

	for _, msg := range messages {
		err = Wrap(err, msg)
	}

	return err
}

func New(msg string) error {
	return &WithMessage{
		msg:   msg,
		cause: nil,
	}
}

func Wrap(err error, msg string) error {
	if err == nil {
		return nil
	}

	return &WithMessage{
		msg:   msg,
		cause: err,
	}
}
