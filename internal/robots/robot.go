package robots

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/myerr"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/nulltypes"
)

var (
	ErrBadJSON         = myerr.New("Bad body parameters")
	ErrBadQueryParams  = myerr.New("Bad query params provided")
	ErrForbidden       = myerr.New("Bad ownerID")
	ErrScanFailed      = myerr.New("Failed to scan robot from row")
	ErrNoRows          = myerr.New("No rows returned")
	ErrNotFound        = myerr.New("Robots search failed")
	ErrInvalidRobotID  = myerr.New("Failed to find robot with ID")
	ErrAlreadyActive   = myerr.New("Robot is already active")
	ErrAlreadyInactive = myerr.New("Robot is already deactivated")
	ErrAlreadyWorking  = myerr.New("Robot is working already")

	ErrOperationFailed = myerr.New("Failed to perform operation on Robot instance")
)

type Robot struct {
	RobotID       uuid.UUID      `json:"robot_id"`
	OwnerUserID   uuid.UUID      `json:"owner_user_id"`
	ParentRobotID uuid.UUID      `json:"parent_robot_id"`
	IsFavorite    bool           `json:"is_favourite"` //nolint:misspell
	IsActive      bool           `json:"is_active"`
	Ticker        string         `json:"ticker,omitempty"`
	BuyPrice      float64        `json:"buy_price,omitempty"`
	SellPrice     float64        `json:"sell_price,omitempty"`
	PlanStart     nulltypes.Time `json:"plan_start,omitempty"`
	PlanEnd       nulltypes.Time `json:"plan_end,omitempty"`
	PlanYield     float64        `json:"plan_yield,omitempty"`
	FactYield     float64        `json:"fact_yield,omitempty"`
	DealsCount    int64          `json:"deals_count,omitempty"`
	ActivatedAt   nulltypes.Time `json:"-"`
	DeactivatedAt nulltypes.Time `json:"-"`
	CreatedAt     nulltypes.Time `json:"-"`
	DeletedAt     nulltypes.Time `json:"-"`
	IsSelling     bool           `json:"-"`
}

type Storage interface {
	Create(r *Robot) error
	Delete(id uuid.UUID) error
	FindByID(id uuid.UUID) (*Robot, error)
	GetAll() ([]*Robot, error)
	GetAllIDs() ([]uuid.UUID, error)

	FilterRobotsByUser(userID uuid.UUID) ([]*Robot, error)
	FilterRobotsByTicker(ticker string) ([]*Robot, error)
	FilterRobotsByTickerAndUser(ticker string, userID uuid.UUID) ([]*Robot, error)

	Activate(id uuid.UUID) (*Robot, error)
	Deactivate(id uuid.UUID) (*Robot, error)
	Update(r *Robot) (*Robot, error)

	FilterIDsByUser(userID uuid.UUID) (robotIDs []uuid.UUID, err error)
	FilterIDsByTicker(ticker string) (robotsIDs []uuid.UUID, err error)
	FilterIDsByTickerAndUser(ticker string, userID uuid.UUID) (robotsIDs []uuid.UUID, err error)
	FindReadyRobots() (map[string][]*Robot, error)
}

func (r *Robot) CreateNewRobot(body []byte, ownerID uuid.UUID, storage Storage) error {
	if err := checkRobotJSON(body); err != nil {
		return myerr.Wrap(err, ErrBadJSON.Error())
	}

	err := json.Unmarshal(body, r)
	if err != nil {
		return myerr.Wrap(err, ErrBadJSON.Error())
	}

	r.RobotID = uuid.New()
	r.OwnerUserID = ownerID
	r.CreatedAt.Time = time.Now()

	if r.IsActive {
		r.ActivatedAt.Time = time.Now()
	}

	err = storage.Create(r)
	if err != nil {
		return myerr.Wrap(err, ErrOperationFailed.Error())
	}

	return nil
}

func (r Robot) String() string {
	return fmt.Sprintf("robot_id: %s, owner_id: %s, parent_robot_id: %s, is_selling:%t, deals_count: %d",
		r.RobotID.String(), r.OwnerUserID.String(), r.ParentRobotID.String(), r.IsSelling, r.DealsCount)
}

func (r *Robot) IsWorkingNow() bool {
	if !r.PlanStart.Valid || !r.PlanEnd.Valid {
		return false
	}

	return r.PlanStart.Time.Before(time.Now()) && r.PlanEnd.Time.After(time.Now())
}

func DeleteRobot(id, userID uuid.UUID, storage Storage) error {
	r, err := storage.FindByID(id)
	if err != nil {
		return myerr.Wrap(err, "failed to find robot with id="+id.String())
	}

	if r.OwnerUserID != userID {
		return myerr.Wrap(err, ErrForbidden.Error())
	}

	err = storage.Delete(id)
	if err != nil {
		return myerr.Wrap(err, ErrOperationFailed.Error())
	}

	return nil
}

// Filter robots according to userID and ticker.
func FilterRobots(userIDString, ticker string, storage Storage) ([]*Robot, error) {
	if userIDString == "" {
		if ticker == "" {
			return storage.GetAll()
		}

		return storage.FilterRobotsByTicker(ticker)
	}

	userID, err := uuid.Parse(userIDString)
	if err != nil {
		return nil, myerr.WithMessages(err, "bad userID provided", ErrBadQueryParams.Error())
	}

	if ticker == "" {
		return storage.FilterRobotsByUser(userID)
	}

	return storage.FilterRobotsByTickerAndUser(ticker, userID)
}

func MakeFavorite(userID, robotID uuid.UUID, storage Storage) (*Robot, error) {
	r, er := storage.FindByID(robotID)
	if er != nil {
		return nil, myerr.Wrap(er, ErrInvalidRobotID.Error())
	}

	r.OwnerUserID = userID
	r.ParentRobotID = robotID
	r.IsFavorite = true
	r.IsActive = false

	err := storage.Create(r)
	if err != nil {
		return nil, myerr.Wrap(err, ErrOperationFailed.Error())
	}

	return r, nil
}

func Activate(robotID, userID uuid.UUID, storage Storage) (*Robot, error) {
	r, err := storage.FindByID(robotID)
	if err != nil {
		return nil, myerr.Wrap(err, ErrNotFound.Error())
	}

	if r.IsActive {
		return nil, ErrAlreadyActive
	}

	if r.OwnerUserID != userID {
		return nil, ErrForbidden
	}

	if r.IsWorkingNow() {
		return nil, ErrAlreadyWorking
	}

	return storage.Activate(robotID)
}

func Deactivate(robotID, userID uuid.UUID, storage Storage) (*Robot, error) {
	r, err := storage.FindByID(robotID)
	if err != nil {
		return nil, myerr.Wrap(err, ErrNotFound.Error())
	}

	if !r.IsActive {
		return nil, ErrAlreadyInactive
	}

	if r.OwnerUserID != userID {
		return nil, ErrForbidden
	}

	if r.IsWorkingNow() {
		return nil, ErrAlreadyWorking
	}

	return storage.Deactivate(robotID)
}

func Update(body []byte, userID, robotID uuid.UUID, storage Storage) (*Robot, error) {
	if err := checkRobotJSON(body); err != nil {
		return nil, myerr.Wrap(err, ErrBadJSON.Error())
	}

	var r Robot

	err := json.Unmarshal(body, &r)
	if err != nil {
		return nil, myerr.Wrap(err, ErrBadJSON.Error())
	}

	//if r.IsWorkingNow() {
	//	return nil, ErrAlreadyWorking
	//}

	r.OwnerUserID = userID
	r.RobotID = robotID

	return storage.Update(&r)
}

func FilterIDS(userIDString, ticker string, storage Storage) ([]uuid.UUID, error) {
	if userIDString == "" {
		if ticker == "" {
			return storage.GetAllIDs()
		}

		return storage.FilterIDsByTicker(ticker)
	}

	userID, err := uuid.Parse(userIDString)
	if err != nil {
		return nil, myerr.WithMessages(err, "bad userID provided", ErrBadQueryParams.Error())
	}

	if ticker == "" {
		return storage.FilterIDsByUser(userID)
	}

	return storage.FilterIDsByTickerAndUser(ticker, userID)
}

func checkRobotJSON(body []byte) error {
	m := make(map[string]interface{})

	err := json.Unmarshal(body, &m)
	if err != nil {
		return err
	}

	//if _, ok := m["owner_user_id"]; !ok {
	//	return myerr.New("owner_user_id field isn't provided")
	//}

	if _, ok := m["is_favourite"]; !ok { //nolint
		return myerr.New("is_favorite field isn't provided")
	}

	if _, ok := m["is_active"]; !ok {
		return myerr.New("is_active field isn't provided")
	}

	return nil
}
