package users

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        uuid.UUID     `json:"-"`
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	Birthday  JSONBirthDate `json:"birthday"`
	Email     string        `json:"email"`
	Password  string        `json:"password"`
	CreatedAt time.Time     `json:"-"`
	UpdatedAt time.Time     `json:"-"`
}

type Storage interface {
	Create(u *User) error
	Find(id uuid.UUID) (*User, error)
	FindByEmail(email string) (*User, error)
	Update(newUser *User) error
}

type JSONBirthDate time.Time

func (j *JSONBirthDate) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")

	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return fmt.Errorf("failed to unmarshal birth day from JSON: %s", err)
	}

	*j = JSONBirthDate(t)

	return nil
}

func (j JSONBirthDate) Equals(anotherBirthDate JSONBirthDate) bool {
	t := time.Time(j)
	anotherT := time.Time(anotherBirthDate)

	return t.Equal(anotherT)
}

func (j JSONBirthDate) IsZero() bool {
	t := time.Time(j)

	return t.IsZero()
}

func (j JSONBirthDate) Format(s string) string {
	t := time.Time(j)

	return t.Format(s)
}

func (j JSONBirthDate) String() string {
	year, month, day := time.Time(j).Date()

	return fmt.Sprintf("%d-%d-%d", year, month, day)
}

func (u User) IsValid() bool {
	return u.FirstName != "" && u.LastName != "" && u.Email != ""
}

func (u User) String() string {
	return fmt.Sprintf(
		"id: %s, last_name: %s, first_name: %s, birthday: %s, email: %s",
		u.ID.String(), u.LastName, u.FirstName, u.Birthday.String(), u.Email)
}

func (u *User) CreateNewUser(body []byte) error {
	err := json.Unmarshal(body, u)
	if err != nil {
		return err
	}

	if !u.IsValid() {
		fmt.Println("Got users: ", u.String())

		return fmt.Errorf("unmarshalled bad users: \n%s", u.String())
	}

	u.ID = uuid.New()
	u.CreatedAt = time.Now()
	u.UpdatedAt = u.CreatedAt

	return nil
}

func (u User) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("{\n\t\"first_name\": \"%s\",\n\t\"last_name\": \"%s\",\n\t\"birthday\": \"%s\",\n\t"+
		"\"email\": \"%s\"\n}",
		u.FirstName, u.LastName, u.Birthday.String(), u.Email)), nil
}

func (u *User) UpdateUser(anotherUser User) error {
	if anotherUser.Email != "" {
		u.Email = anotherUser.Email
	}

	if anotherUser.Password != "" {
		u.Password = anotherUser.Password
	}

	if !anotherUser.Birthday.IsZero() {
		u.Birthday = anotherUser.Birthday
	}

	if anotherUser.FirstName != "" {
		u.FirstName = anotherUser.FirstName
	}

	if anotherUser.LastName != "" {
		u.LastName = anotherUser.LastName
	}

	return nil
}

func (u User) CheckCredentials(hashedUser User) error {
	if u.Email != hashedUser.Email {
		return fmt.Errorf("incorrect emails: %s and %s", u.Email, hashedUser.Email)
	}

	return bcrypt.CompareHashAndPassword([]byte(hashedUser.Password), []byte(u.Password))
}
