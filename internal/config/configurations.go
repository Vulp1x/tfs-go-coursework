package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/Vulp1x/tfs-go-coursework/internal/postgres"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/sessions"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
	"gopkg.in/yaml.v2"
)

// Config represents application configuration.
type Config struct {
	Port     string                 `yaml:"port"`
	Logger   logger.Configuration   `yaml:"logger"`
	Postgres postgres.Configuration `yaml:"postgres"`
	Security sessions.Configuration `yaml:"session"`
}

// ParseConfiguration parses configuration from config.yml.
func (c *Config) ParseConfiguration() error {
	configFile, err := os.Open("config.yml")
	if err != nil {
		return fmt.Errorf("failed to open config file: %w", err)
	}

	data, _ := ioutil.ReadAll(configFile)

	return yaml.Unmarshal(data, c)
}
