package middlewares

import (
	"context"
	"encoding/json"
	"net/http"
	"reflect"
	"runtime/debug"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/sessions"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/users"
)

type contextKey string

func (c contextKey) String() string {
	return "middlewares context value " + string(c)
}

func (c contextKey) Write(r *http.Request, val interface{}) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), c, val))
}

const (
	// claimsRequestKey key for using in context.
	claimsRequestKey contextKey = "Claims"
	// tokenRequestKey key for using in context.
	tokenRequestKey contextKey = "Token"
)

// CheckSession check sesssion middleware.
func CheckSession(securityConfig sessions.Configuration) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			tokenString := r.Header.Get("Bearer")
			if tokenString == "" {
				Error(w, r, http.StatusUnauthorized, "No token provided")

				return
			}

			token, err := jwt.ParseWithClaims(tokenString, &sessions.SessionClaims{}, securityConfig.KeyFunc)
			if err != nil {
				InternalError(w, r, "Failed to parse token: %v", err)

				return
			}

			if claims, ok := token.Claims.(*sessions.SessionClaims); ok && token.Valid {
				LogEntrySetField(r, "user_id", claims.UserID)
				GetLogEntry(r).Logger.Debugf("Successfully checked token")

				next.ServeHTTP(w, claimsRequestKey.Write(r, claims))
			} else {
				InternalError(w, r, "Token is valid: %t or Claims are SessionClaims: %v", token.Valid, reflect.TypeOf(token.Claims))

				return
			}
		}

		return http.HandlerFunc(fn)
	}
}

// CheckCredentials check user credentials middleware.
func CheckCredentials(repo users.Storage, conf sessions.Configuration) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			reqLogger := GetLogEntry(r).Logger
			body := GetBody(r)

			bodyUser := make(map[string]string)

			err := json.Unmarshal(body, &bodyUser)
			if err != nil {
				BadRequest(w, r, "Failed to unmarshal User json: %s ", err)

				return
			}

			u, err2 := repo.FindByEmail(bodyUser["email"])
			if err2 != nil {
				reqLogger.Infof("No user with email: %s", bodyUser["email"])
				http.Error(w, "No user with email: "+bodyUser["email"], http.StatusNotFound)

				return
			}

			if bodyUser["password"] != u.Password {
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)

				return
			}

			LogEntrySetField(r, "user_id", u.ID)
			claims := sessions.NewSession(u.ID, conf.TokenDuration)

			signedToken, tokenErr := claims.GenerateSignedToken(conf.SigningKey)
			if tokenErr != nil {
				InternalError(w, r, "Failed to create signed token: %v", tokenErr)

				return
			}

			reqLogger.Debugf("Successfully checked credentials for user: %s", u.ID.String())

			r = claimsRequestKey.Write(r, claims)
			next.ServeHTTP(w, tokenRequestKey.Write(r, signedToken))
		}

		return http.HandlerFunc(fn)
	}
}

// CheckAccessToURL checks if user could access following url.
func CheckAccessToURL(repo users.Storage) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			reqLogger := GetLogEntry(r).Logger
			ses := GetClaims(r)

			id, err := uuid.Parse(chi.URLParam(r, "userID"))
			if err != nil {
				Error(w, r, http.StatusBadRequest, "Failed to parse userID from url")

				return
			}

			if id != ses.UserID {
				reqLogger.Warnf("Session is for another users: %d %d ", "url_id", id, "token_id", ses.UserID)
				http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)

				return
			}

			dbUser, err2 := repo.Find(id)
			if err2 != nil {
				Error(w, r, http.StatusNotFound, "Failed to get users with id %s: %v", id.String(), err)

				return
			}

			reqLogger.Debugf("Successfully checked credentials for dbUser: %s", dbUser.ID.String())

			next.ServeHTTP(w, dbUserRequestKey.Write(r, dbUser))
		}

		return http.HandlerFunc(fn)
	}
}

// GetToken is used to get User JSON Web Token from request context.
func GetToken(r *http.Request) string {
	signedToken, ok := r.Context().Value(tokenRequestKey).(string)
	if !ok {
		panic("can`t get signed token from context when expected" +
			string(debug.Stack()))
	}

	return signedToken
}

// GetClaims is used to get claims(userID etc) from request context.
func GetClaims(r *http.Request) *sessions.SessionClaims {
	claims, ok := r.Context().Value(claimsRequestKey).(*sessions.SessionClaims)
	if !ok {
		panic("can`t get claims from context when expected" +
			string(debug.Stack()))
	}

	return claims
}
