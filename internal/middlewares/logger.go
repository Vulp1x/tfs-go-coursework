package middlewares

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
)

// tokenRequestKey key for using in context.
const loggerConfigRequestKey contextKey = "Logger"

var _ middleware.LogEntry = &StructuredLoggerEntry{}

type StructuredLogger struct {
	Logger logger.Logger
}

func (l *StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	log, _ := logger.NewLogger(GetLoggerConfig(r))
	entry := &StructuredLoggerEntry{Logger: log}
	logFields := logger.Fields{}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}

	logFields["http_scheme"] = scheme
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	entry.Logger = entry.Logger.WithFields(logFields)

	entry.Logger.Infof("request started")

	return entry
}

type StructuredLoggerEntry struct {
	Logger logger.Logger
}

func (l *StructuredLoggerEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	l.Logger = l.Logger.WithFields(logger.Fields{
		"resp_status": status, "resp_bytes_length": bytes,
		"resp_elapsed_ms": float64(elapsed.Milliseconds()),
	})

	l.Logger.Infof("request complete")
}

func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logger.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}

// Helper methods used by the application to get the request-scoped
// logger entry and set additional fields between handlers.
//
// This is a useful pattern to use to set state on the entry as it
// passes through the handler chain, which at any point can be logged
// with a call to .Print(), .Info(), etc.

func GetLogEntry(r *http.Request) *StructuredLoggerEntry {
	entry := middleware.GetLogEntry(r).(*StructuredLoggerEntry)

	return entry
}

func LogEntrySetField(r *http.Request, key string, value interface{}) {
	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithFields(logger.Fields{key: value})
	}
}

func LogEntrySetFields(r *http.Request, fields map[string]interface{}) {
	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithFields(fields)
	}
}

func AddLoggerConfig(loggerConfig logger.Configuration) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, loggerConfigRequestKey.Write(r, loggerConfig))
		}

		return http.HandlerFunc(fn)
	}
}

func GetLoggerConfig(r *http.Request) logger.Configuration {
	conf, ok := r.Context().Value(loggerConfigRequestKey).(logger.Configuration)
	if !ok {
		panic("can`t get logger config from context when expected")
	}

	return conf
}

// InternalError helper for sending error.
func InternalError(
	w http.ResponseWriter,
	r *http.Request,
	msg string, args ...interface{}) {
	reqLogger := GetLogEntry(r).Logger
	reqLogger.Errorf(msg, args...)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// BadRequest helper for sending error.
func BadRequest(
	w http.ResponseWriter,
	r *http.Request,
	msg string, args ...interface{}) {
	reqLogger := GetLogEntry(r).Logger
	reqLogger.Errorf(msg, args...)

	http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
}

// Error helper for sending error.
func Error(
	w http.ResponseWriter,
	r *http.Request,
	code int,
	msg string, args ...interface{}) {
	reqLogger := GetLogEntry(r).Logger
	reqLogger.Errorf(msg, args...)

	http.Error(w, http.StatusText(code), code)
}
