package middlewares

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
)

const (
	UserID  = "userID"
	RobotID = "robotID"
)

type ctxKeyUserID int

// UserIDKey is the key that holds user's ID in a request context.
const UserIDKey ctxKeyUserID = 0

type ctxKeyRobotID int

// RobotIDKey is the key that holds user's ID in a request context.
const RobotIDKey ctxKeyRobotID = 0

func AddIDFromURL(idName string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			reqLogger := GetLogEntry(r).Logger

			if idName != UserID && idName != RobotID {
				reqLogger.Fatalf("Bad idName used in middleware: %s", idName)

				return
			}

			idString := chi.URLParam(r, idName)

			id, err := uuid.Parse(idString)
			if err != nil {
				reqLogger.Infof("Failed to parse id from url: %v", err)
				http.Error(w, "Bad id provided: "+idString, http.StatusBadRequest)

				return
			}

			if idName == RobotID {
				ctx = context.WithValue(ctx, RobotIDKey, id)
			} else {
				ctx = context.WithValue(ctx, UserIDKey, id)
			}

			reqLogger.Infof("Added %s to request: %d", idName, id)
			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

// GetRobotID returns a robot's ID from the given context if one is present.
// Returns zero if a robot ID cannot be found.
func GetRobotID(r *http.Request) uuid.UUID {
	robID, ok := r.Context().Value(RobotIDKey).(uuid.UUID)
	if !ok {
		panic("can`t get robotID from context when expected")
	}

	return robID
}

// GetUserID returns a user's ID from the given context if one is present.
// Returns zero if a user ID cannot be found.
func GetUserID(ctx context.Context) uuid.UUID {
	if ctx == nil {
		return uuid.Nil
	}

	userID, _ := ctx.Value(UserIDKey).(uuid.UUID)

	return userID
}
