package postgres

import (
	"database/sql"
	"fmt"
	"log"

	migrate "github.com/rubenv/sql-migrate"
)

// Configuration represents database configuration.
type Configuration struct {
	Host          string `yaml:"host"`
	Port          string `yaml:"port"`
	UserName      string `yaml:"user"`
	Password      string `yaml:"password"`
	Database      string `yaml:"database"`
	SSLMode       string `yaml:"ssl mode"`
	SearchPath    string `yaml:"search path"`
	MigrationsDir string `yaml:"migrations dir"`
}

// NewDB returns new database connection.
func NewDB(postgresConfig Configuration) (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		postgresConfig.Host, postgresConfig.Port, postgresConfig.UserName, postgresConfig.Database,
		postgresConfig.Password))
	if err != nil {
		return nil, err
	}

	// Read migrations from a folder:
	migrations := &migrate.FileMigrationSource{
		Dir: postgresConfig.MigrationsDir,
	}

	n, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
	if err != nil {
		log.Fatalf("Couldnt apply migrations, applied %d migrations!\nerr=%s", n, err)
	}

	return db, err
}

type sqlScanner interface {
	Scan(dest ...interface{}) error
}
