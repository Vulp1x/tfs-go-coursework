package postgres

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/users"
)

var (
	_                     users.Storage = &UserStorage{}
	ErrEmailIsAlreadyUsed               = errors.New("same email already registered")
)

type UserStorage struct {
	statementStorage

	createStmt      *sql.Stmt
	findStmt        *sql.Stmt
	findByEmailStmt *sql.Stmt
	updateStmt      *sql.Stmt
}

func NewUserStorage(db *sql.DB) (*UserStorage, error) {
	s := &UserStorage{statementStorage: newStatementsStorage(db)}

	stmts := []stmt{
		{Query: createUserQuery, Dst: &s.createStmt},
		{Query: findUserQuery, Dst: &s.findStmt},
		{Query: findUserByEmailQuery, Dst: &s.findByEmailStmt},
		{Query: updateUserQuery, Dst: &s.updateStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

const userFields = "first_name, last_name, birthday, email, password, created_at, updated_at"

func scanUser(scanner sqlScanner, u *users.User) error {
	return scanner.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Birthday, &u.Email, &u.Password,
		&u.CreatedAt, &u.UpdatedAt)
}

const createUserQuery = "INSERT INTO users(id, " + userFields + ") VALUES ($1, $2, $3, $4, $5, $6, now(), now()) RETURNING id"

func (s *UserStorage) Create(u *users.User) error {
	if err := s.createStmt.QueryRow(u.ID, u.FirstName, u.LastName, u.Birthday.String(), u.Email, u.Password).
		Scan(&u.ID); err != nil {
		if postgresErr, ok := err.(*pq.Error); ok {
			if postgresErr.Constraint == "users__email_uniq" {
				return ErrEmailIsAlreadyUsed
			}
		}

		return errors.Wrap(err, "can't exec query")
	}

	return nil
}

const findUserQuery = "SELECT id, " + userFields + " FROM users WHERE id=$1"

func (s *UserStorage) Find(id uuid.UUID) (*users.User, error) {
	var u users.User

	row := s.findStmt.QueryRow(id)
	if err := scanUser(row, &u); err != nil {
		return nil, errors.Wrap(err, "can't scan user")
	}

	return &u, nil
}

const findUserByEmailQuery = "SELECT id, " + userFields + " FROM users WHERE email=$1"

func (s *UserStorage) FindByEmail(email string) (*users.User, error) {
	var u users.User

	row := s.findByEmailStmt.QueryRow(email)
	if err := scanUser(row, &u); err != nil {
		return nil, errors.Wrap(err, "can't scan user")
	}

	return &u, nil
}

const updateUserQuery = "UPDATE users" +
	" SET first_name=$1, last_name=$2, birthday=$3, email=$4, password=$5, updated_at=now()" +
	"WHERE id=$6" +
	"RETURNING id"

func (s *UserStorage) Update(newUser *users.User) error {
	if err := s.updateStmt.QueryRow(newUser.FirstName, newUser.LastName, newUser.Birthday.String(),
		newUser.Email, newUser.Password, newUser.ID).Scan(&newUser.ID); err != nil {
		return err
	}

	return nil
}
