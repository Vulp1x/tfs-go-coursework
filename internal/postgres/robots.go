package postgres

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/robots"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/myerr"
)

var _ robots.Storage = &RobotStorage{}

type RobotStorage struct {
	statementStorage

	createStmt *sql.Stmt
	deleteStmt *sql.Stmt

	findStmt   *sql.Stmt
	getAllStmt *sql.Stmt

	activateStmt   *sql.Stmt
	deactivateStmt *sql.Stmt
	updateStmt     *sql.Stmt

	filterRobotsByTickerStmt        *sql.Stmt
	filterRobotsByUserStmt          *sql.Stmt
	filterRobotsByTickerAndUserStmt *sql.Stmt

	filterIDByTickerStmt        *sql.Stmt
	filterIDByUserStmt          *sql.Stmt
	filterIDByTickerAndUserStmt *sql.Stmt
	getAllIDSStmt               *sql.Stmt

	findReadyStmt *sql.Stmt
}

func NewRobotStorage(db *sql.DB) (*RobotStorage, error) {
	s := &RobotStorage{statementStorage: newStatementsStorage(db)}

	stmts := []stmt{
		{Query: createRobotQuery, Dst: &s.createStmt},
		{Query: deleteRobotQuery, Dst: &s.deleteStmt},
		{Query: findByIDRobotQuery, Dst: &s.findStmt},

		{Query: activateRobotQuery, Dst: &s.activateStmt},
		{Query: deactivateRobotQuery, Dst: &s.deactivateStmt},
		{Query: updateRobotQuery, Dst: &s.updateStmt},

		{Query: filterRobotsByOwnerIDQuery, Dst: &s.filterRobotsByUserStmt},
		{Query: filterRobotsByTickerQuery, Dst: &s.filterRobotsByTickerStmt},
		{Query: filterRobotsByTickerAndUserQuery, Dst: &s.filterRobotsByTickerAndUserStmt},
		{Query: getAllRobotsQuery, Dst: &s.getAllStmt},

		{Query: filterIDByUserQuery, Dst: &s.filterIDByUserStmt},
		{Query: filterIDByTickerQuery, Dst: &s.filterIDByTickerStmt},
		{Query: filterIDByTickerAndUserQuery, Dst: &s.filterIDByTickerAndUserStmt},
		{Query: getAllIDsQuery, Dst: &s.getAllIDSStmt},

		{Query: findReadyRobotsQuery, Dst: &s.findReadyStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

const findReadyRobotsQuery = "SELECT robot_id, " + robotFields + " FROM robots " +
	"WHERE is_active=true AND plan_start < now() AND plan_end > now() AND deals_count != 0"

func (s RobotStorage) FindReadyRobots() (map[string][]*robots.Robot, error) {
	rows, err := s.findReadyStmt.Query()
	if err != nil {
		return nil, myerr.Wrap(err, "failed to get all ready robots")
	}

	if rows.Err() != nil {
		return nil, myerr.Wrap(rows.Err(), "failed to get all ready robots")
	}

	defer rows.Close()

	robIDs := make(map[string][]*robots.Robot)

	for rows.Next() {
		var r robots.Robot

		if err := scanRobot(rows, &r); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robIDs[r.Ticker] = append(robIDs[r.Ticker], &r)
	}

	return robIDs, nil
}

const getAllIDsQuery = "SELECT robot_id FROM robots"

func (s RobotStorage) GetAllIDs() ([]uuid.UUID, error) {
	rows, err := s.getAllIDSStmt.Query()
	if err != nil {
		return nil, myerr.Wrap(err, "failed to get all robots id")
	}

	if rows.Err() != nil {
		return nil, myerr.Wrap(rows.Err(), "failed to get all robots id")
	}

	defer rows.Close()

	var robIDs []uuid.UUID

	var id uuid.UUID

	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robIDs = append(robIDs, id)
	}

	return robIDs, nil
}

const filterIDByUserQuery = "SELECT robot_id FROM robots WHERE owner_user_id=$1"

func (s RobotStorage) FilterIDsByUser(userID uuid.UUID) (robotIDs []uuid.UUID, err error) {
	rows, err := s.filterIDByUserStmt.Query(userID)
	if err != nil || rows.Err() != nil {
		return nil, myerr.WithMessages(err, "failed to find robots with owner_user_id="+userID.String(),
			robots.ErrNotFound.Error())
	}

	defer rows.Close()

	var id uuid.UUID

	var robIDs []uuid.UUID

	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robIDs = append(robIDs, id)
	}

	return robIDs, nil
}

const filterIDByTickerQuery = "SELECT robot_id FROM robots WHERE ticker=$1"

func (s RobotStorage) FilterIDsByTicker(ticker string) (robotsIDs []uuid.UUID, err error) {
	rows, e := s.filterIDByTickerStmt.Query(ticker)
	if e != nil {
		return nil, myerr.WithMessages(e, "failed to find robots with ticker="+ticker,
			robots.ErrNotFound.Error())
	}

	if rows.Err() != nil {
		return nil, myerr.WithMessages(rows.Err(), "failed to find robots with ticker="+ticker,
			robots.ErrNotFound.Error())
	}

	defer rows.Close()

	var id uuid.UUID

	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robotsIDs = append(robotsIDs, id)
	}

	return robotsIDs, nil
}

const filterIDByTickerAndUserQuery = "SELECT robot_id FROM robots " +
	"WHERE ticker=$1 AND owner_user_id=$2"

func (s RobotStorage) FilterIDsByTickerAndUser(ticker string, userID uuid.UUID) (robotsIDs []uuid.UUID, err error) {
	rows, e := s.filterIDByTickerAndUserStmt.Query(ticker, userID)
	if e != nil {
		return nil, myerr.WithMessages(e, "failed to find robots with owner_user_id="+userID.String(),
			robots.ErrNotFound.Error())
	}

	if rows.Err() != nil {
		return nil, myerr.WithMessages(rows.Err(), "failed to find robots with owner_user_id="+userID.String(),
			robots.ErrNotFound.Error())
	}

	defer rows.Close()

	var id uuid.UUID

	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robotsIDs = append(robotsIDs, id)
	}

	return robotsIDs, nil
}

const robotFields = "owner_user_id, parent_robot_id, is_favorite, is_active, ticker, buy_price, sell_price," +
	"plan_start, plan_end, plan_yield, fact_yield, deals_count, activated_at, deactivated_at, created_at, deleted_at," +
	" is_selling"

const createRobotQuery = "INSERT into robots(robot_id, " + robotFields + ") " +
	"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, 0, 0,  null, null, now(), null, false)" +
	"RETURNING robot_id;"

func (s RobotStorage) Create(r *robots.Robot) error {
	err := s.createStmt.QueryRow(r.RobotID, r.OwnerUserID, r.ParentRobotID, r.IsFavorite, r.IsActive, r.Ticker,
		r.BuyPrice, r.SellPrice, r.PlanStart, r.PlanEnd, r.PlanYield).Scan(&r.RobotID)

	return err
}

const getAllRobotsQuery = "SELECT robot_id, " + robotFields + " FROM robots;"

func (s RobotStorage) GetAll() ([]*robots.Robot, error) {
	rows, err := s.getAllStmt.Query()
	if err != nil {
		return nil, myerr.Wrap(err, "failed to get all robots")
	}

	if rows.Err() != nil {
		return nil, myerr.Wrap(rows.Err(), "failed to get all robots")
	}

	defer rows.Close()

	var robs []*robots.Robot

	for rows.Next() {
		var r robots.Robot
		if err := scanRobot(rows, &r); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robs = append(robs, &r)
	}

	return robs, nil
}

const filterRobotsByTickerAndUserQuery = "SELECT robot_id, " + robotFields + " FROM robots " +
	"WHERE ticker=$1 AND owner_user_id=$2;"

func (s RobotStorage) FilterRobotsByTickerAndUser(ticker string, userID uuid.UUID) ([]*robots.Robot, error) {
	rows, err := s.filterRobotsByTickerAndUserStmt.Query(ticker, userID)
	if err != nil {
		return nil, myerr.WithMessages(err, "failed to find robot with ticker "+ticker+
			" and owner "+userID.String(), robots.ErrNotFound.Error())
	}

	if rows.Err() != nil {
		return nil, myerr.WithMessages(rows.Err(), "failed to find robot with ticker "+ticker+
			" and owner "+userID.String(), robots.ErrNotFound.Error())
	}

	defer rows.Close()

	var robs []*robots.Robot

	for rows.Next() {
		var r robots.Robot
		if err := scanRobot(rows, &r); err != nil {
			return nil, myerr.Wrap(err, "failed to scan robot from row")
		}

		robs = append(robs, &r)
	}

	return robs, nil
}

const updateRobotQuery = "UPDATE robots SET owner_user_id=$1, parent_robot_id=$2, " +
	"is_favorite=$3, is_active=$4, ticker=$5, buy_price=$6, sell_price=$7, plan_start=$8, plan_end=$9, " +
	"plan_yield=$10, fact_yield=$11, deals_count=$12, activated_at=$13, deactivated_at=$14, " +
	"created_at=$15, deleted_at=$16, is_selling=$17 WHERE robot_id=$18" +
	"RETURNING robot_id, " + robotFields + ";"

func (s RobotStorage) Update(r *robots.Robot) (*robots.Robot, error) {
	var rob robots.Robot

	row := s.updateStmt.QueryRow(r.OwnerUserID, r.ParentRobotID, r.IsFavorite, r.IsActive, r.Ticker,
		r.BuyPrice, r.SellPrice, r.PlanStart, r.PlanEnd, r.PlanYield, r.FactYield, r.DealsCount,
		r.ActivatedAt, r.DeactivatedAt, r.CreatedAt, r.DeletedAt, r.IsSelling, r.RobotID)

	if err := scanRobot(row, &rob); err != nil {
		return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
	}

	return &rob, nil
}

const findByIDRobotQuery = "SELECT robot_id, " + robotFields + " FROM robots WHERE robot_id=$1"

func (s RobotStorage) FindByID(id uuid.UUID) (*robots.Robot, error) {
	var robot robots.Robot

	row := s.findStmt.QueryRow(id)
	if err := scanRobot(row, &robot); err != nil {
		return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
	}

	return &robot, nil
}

const filterRobotsByOwnerIDQuery = "SELECT robot_id, " + robotFields + " FROM robots WHERE owner_user_id=$1"

func (s RobotStorage) FilterRobotsByUser(userID uuid.UUID) ([]*robots.Robot, error) {
	var robs []*robots.Robot

	rows, err := s.filterRobotsByUserStmt.Query(userID)
	if err != nil {
		return nil, myerr.WithMessages(err, "failed to find robots with owner_user_id="+userID.String(),
			robots.ErrNotFound.Error())
	}

	if rows.Err() != nil {
		return nil, myerr.WithMessages(rows.Err(), "failed to find robots with owner_user_id="+userID.String(),
			robots.ErrNotFound.Error())
	}

	defer rows.Close()

	for rows.Next() {
		var r robots.Robot
		if err := scanRobot(rows, &r); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robs = append(robs, &r)
	}

	return robs, nil
}

const filterRobotsByTickerQuery = "SELECT robot_id, " + robotFields + " FROM robots WHERE ticker=$1"

func (s RobotStorage) FilterRobotsByTicker(ticker string) ([]*robots.Robot, error) {
	var robs []*robots.Robot

	rows, err := s.filterRobotsByTickerStmt.Query(ticker)
	if err != nil {
		return nil, myerr.WithMessages(err, "failed to find robots with ticker="+ticker, robots.ErrNotFound.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var r robots.Robot
		if err = scanRobot(rows, &r); err != nil {
			return nil, myerr.Wrap(err, robots.ErrScanFailed.Error())
		}

		robs = append(robs, &r)
	}

	if err = rows.Err(); err != nil {
		return nil, myerr.Wrap(err, robots.ErrNoRows.Error())
	}

	return robs, nil
}

// robots is a view with deleted_at=NULL rows.
const deleteRobotQuery = "UPDATE robots SET deleted_at=now() WHERE robot_id=$1 RETURNING robot_id"

func (s RobotStorage) Delete(id uuid.UUID) error {
	if err := s.deleteStmt.QueryRow(id).Scan(&id); err != nil {
		return myerr.Wrap(err, "failed to soft-delete robot with id = "+id.String())
	}

	return nil
}

const activateRobotQuery = "UPDATE robots SET is_active=true, activated_at=now() WHERE robot_id = $1" +
	"RETURNING robot_id, " + robotFields + " ;"

func (s RobotStorage) Activate(id uuid.UUID) (*robots.Robot, error) {
	var robot robots.Robot

	row := s.activateStmt.QueryRow(id)
	if err := scanRobot(row, &robot); err != nil {
		return nil, myerr.Wrap(err, robots.ErrOperationFailed.Error())
	}

	return &robot, nil
}

const deactivateRobotQuery = "UPDATE robots SET is_active=false, deactivated_at=now() WHERE robot_id = $1" +
	"RETURNING robot_id, " + robotFields + " ;"

func (s RobotStorage) Deactivate(id uuid.UUID) (*robots.Robot, error) {
	var robot robots.Robot

	row := s.deactivateStmt.QueryRow(id)
	if err := scanRobot(row, &robot); err != nil {
		return nil, myerr.Wrap(err, robots.ErrOperationFailed.Error())
	}

	return &robot, nil
}

func scanRobot(scanner sqlScanner, r *robots.Robot) error {
	return scanner.Scan(&r.RobotID, &r.OwnerUserID, &r.ParentRobotID, &r.IsFavorite, &r.IsActive, &r.Ticker,
		&r.BuyPrice, &r.SellPrice, &r.PlanStart, &r.PlanEnd, &r.PlanYield, &r.FactYield, &r.DealsCount,
		&r.ActivatedAt, &r.DeactivatedAt, &r.CreatedAt, &r.DeletedAt, &r.IsSelling)
}
