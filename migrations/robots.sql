-- https://github.com/rubenv/sql-migrate for documentation
-- +migrate Up


create table robots_with_deleted
(
    robot_id        uuid
        CONSTRAINT robots_pk PRIMARY KEY,
    owner_user_id   uuid        not null,
    parent_robot_id uuid     not null,
    is_favorite     boolean,
    ticker          varchar(50) not null,
    buy_price       double precision,
    sell_price      double precision,
    plan_start      timestamp with time zone,
    plan_end        timestamp with time zone,
    plan_yield      double precision,
    fact_yield      double precision,
    deals_count     integer,
    activated_at    timestamp with time zone,
    deactivated_at  timestamp with time zone,
    created_at      timestamp with time zone,
    deleted_at      timestamp with time zone,
    is_active       boolean,
    is_selling      boolean default false

);


create index deleted_at_idx
    on robots_with_deleted (deleted_at);

create view robots
            (robot_id, owner_user_id, parent_robot_id, is_favorite, ticker, buy_price, sell_price, plan_start, plan_end,
             plan_yield, fact_yield, deals_count, activated_at, deactivated_at, created_at, deleted_at, is_active,
             is_selling)
as
SELECT robots_with_deleted.robot_id,
       robots_with_deleted.owner_user_id,
       robots_with_deleted.parent_robot_id,
       robots_with_deleted.is_favorite,
       robots_with_deleted.ticker,
       robots_with_deleted.buy_price,
       robots_with_deleted.sell_price,
       robots_with_deleted.plan_start,
       robots_with_deleted.plan_end,
       robots_with_deleted.plan_yield,
       robots_with_deleted.fact_yield,
       robots_with_deleted.deals_count,
       robots_with_deleted.activated_at,
       robots_with_deleted.deactivated_at,
       robots_with_deleted.created_at,
       robots_with_deleted.deleted_at,
       robots_with_deleted.is_active,
       robots_with_deleted.is_selling
FROM robots_with_deleted
WHERE robots_with_deleted.deleted_at IS NULL;


-- +migrate Down

DROP TABLE robots_with_deleted;
