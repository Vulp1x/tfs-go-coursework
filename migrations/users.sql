-- https://github.com/rubenv/sql-migrate for documentation
-- +migrate Up


create table users
(
    id         UUID
        CONSTRAINT users_pk PRIMARY KEY,
    last_name  text,
    first_name text,
    birthday   date,
    email      text
        constraint users_email_key
            unique,
    password   text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE users
    ADD CONSTRAINT users__email_uniq UNIQUE (email);

-- +migrate Down

DROP TABLE users;
