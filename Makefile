SHELL:=/bin/bash
#https://habr.com/ru/post/461467/ something interesting
APP_NAME ?= tfs_coursework
DB_SCHEMA ?= $(APP_NAME)
POSTGRES_CONTAINER ?= postgres-container
NGINX_CONTAINER ?= mnginx-container

GOFILES=$(shell find -iname '*.go')
TMPDIR ?= $(shell dirname $$(mktemp -u))
BIN=bin

CREATEBINDIR := $(shell mkdir $(BIN) &> /dev/null )
CREATETMPDIR := $(shell mkdir tmp &> /dev/null )

GOBIN = $(GOPATH)/bin
GOLINT = $(GOBIN)/golint
GOIMPORTS = $(GOBIN)/goimports
MIGRATE = $(GOBIN)/sql-migrate
PACKR2 = $(GOBIN)/packr2
GOLANGCI = $(GOBIN)/golangci-lint

COVER_FILE = $(TMPDIR)/$(APP_NAME)-coverage.out


$(BIN)/rest-api: $(GOFILES)
	@echo build $(BIN)/rest-api
	@#env CC=clang env CXX=clang++ go build -msan -o $(APP_NAME) ./cmd/rest-api
	@go build -race -o $(BIN)/rest-api ./cmd/rest-api

$(BIN)/grpc-client: $(GOFILES)
	@echo build $(BIN)/grpc-client
	@#env CC=clang env CXX=clang++ go build -msan -o $(APP_NAME) ./cmd/rest-api
	@go build -race -o $(BIN)/grpc-client ./cmd/grpc-client

.PHONY: build_grpc
build_grpc: $(BIN)/grpc-client
	@echo build

.PHONY: run_grpc
run_grpc:  build_grpc
	@sleep 1;
	@echo run
	@$(BIN)/grpc-client


.PHONY: run
run: pg_start build
	@sleep 1;
	@echo run
	@$(BIN)/rest-api &\
	#next strings for stopping containers after app stops\
	apppid=$$!;disown;\
	trap "set -x;kill -HUP $$apppid;docker stop $(POSTGRES_CONTAINER)" EXIT;\
	read -r -d '' _ </dev/tty

.PHONY: build
build: $(BIN)/rest-api
	@echo build

.PHONY: test
check: $(GOLANGCI) $(GOLINT) $(GOIMPORTS) test
	$(GOLANGCI) run  --new-from-rev 0
	go vet ./...
	$(GOLINT) -set_exit_status ./...
	test -z $$(for d in $$(go list -f {{.Dir}} ./...); do gofmt -e -l -w $$d/*.go; done)
	test -z $$(for d in $$(go list -f {{.Dir}} ./...); do goimports -e -l -local $(NAMESPACE) -w $$d/*.go; done)


.PHONY: bench
bench: ## Run benchmarks
	go test ./... -short -bench=. -run="Benchmark*"

.PHONY: pg_start
pg_start: ## Start PostgreSQL service in docker with project compose file
	@echo start $(POSTGRES_CONTAINER)
	@set -e; \
	if [ "$$(docker ps -q --filter 'name=$(POSTGRES_CONTAINER)' --filter 'status=paused')" != "" ]; then \
		set -x; docker unpause $(POSTGRES_CONTAINER);  \
	elif [ "$$(docker ps -q --filter 'name=$(POSTGRES_CONTAINER)' --filter 'status=exited')" != "" ]; then \
		set -x; docker start $(POSTGRES_CONTAINER);  \
	elif [ "$$(docker ps -q --filter 'name=$(POSTGRES_CONTAINER)' --filter 'status=running')" = "" ]; then \
		set -x; docker run --name $(POSTGRES_CONTAINER) -d -p 127.0.0.1:5433:5432 --env 'POSTGRES_HOST_AUTH_METHOD=trust' postgres:12-alpine;  \
	fi;
	@status=1;\
	until [[ $$status -eq 0 ]] ;\
	do\
		sleep 1s;\
		docker exec $(POSTGRES_CONTAINER) bash -c "echo 'create schema if not exists $(DB_SCHEMA)' | psql postgres postgres";\
		status=$$?;\
	done



.PHONY: pg_stop
pg_stop: ## Stop postgres container
	@echo stop $(POSTGRES_CONTAINER)
	@docker stop $(POSTGRES_CONTAINER)

.PHONY: pgcli
pgcli: ## run console to access database
	docker run --rm --network host -ti danihodovic/pgcli postgres://postgres@localhost:5433/postgres\?search_path=$(DB_SCHEMA)


test: ## Run unit (short) tests
	go test -short ./... -coverprofile=$(COVER_FILE)
	go tool cover -func=$(COVER_FILE) | grep ^total

$(COVER_FILE):
	$(MAKE) test

.PHONY: cover
cover: $(COVER_FILE) ## Output coverage in human readable form in html
	go tool cover -html=$(COVER_FILE)
	rm -f $(COVER_FILE)

.PHONY: clean
clean: ## Clean the project from built files
	rm -rf ./$(APP_NAME) bin $(COVER_FILE)
	@echo you can remove docker container
	@docker ps -a --filter "name=$(POSTGRES_CONTAINER)"

.PHONY: clean_all
clean_all: clean ## Clean all including docker containers
	docker stop $(POSTGRES_CONTAINER) || echo
	docker rm $(POSTGRES_CONTAINER) || echo

.PHONY: help
help: ## Print this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


$(GOLINT):
	go install golang.org/x/lint/golint

$(GOIMPORTS):
	go install golang.org/x/tools/cmd/goimports

$(REALIZE):
	go install github.com/oxequa/realize

$(GOLANGCI):
	go install github.com/golangci/golangci-lint/cmd/golangci-lint

.PHONY: tools
tools: $(GOLINT) $(GOIMPORTS) $(MIGRATE) $(REALIZE) $(SWAG) $(PACKR2) $(GOLANGCI)



.PHONY: git_pull
git_pull:
	git pull

.PHONY: deploy_run
deploy_run: git_pull pg_start build
	kill -HUP $$(pidof $(BIN)/rest-api) | echo none to kill
	sleep 3;
	$(BIN)/rest-api > tmp/stdout & disown
