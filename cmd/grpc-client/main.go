package main

import (
	"fmt"
	"log"

	"gitlab.com/Vulp1x/tfs-go-coursework/internal/config"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/postgres"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
)

func main() {
	grpcHub := initGrpcHub()

	fmt.Println("Running grpc server")

	go grpcHub.Run()
	grpcHub.SaveRobots()
}
func initGrpcHub() *Hub {
	conf := &config.Config{}

	err := conf.ParseConfiguration()
	if err != nil {
		log.Fatal("Failed to parse configuration: ", err)
	}

	newLogger, err := logger.NewLogger(conf.Logger)
	if err != nil {
		log.Panic("Could not instantiate log: ", err)
	}

	db, err := postgres.NewDB(conf.Postgres)
	if err != nil {
		log.Panic("Could not connect to database")
	}

	robStorage, err := postgres.NewRobotStorage(db)
	if err != nil {
		newLogger.Fatalf("Failed to create new Robot storage: %s", err)
	}

	grpcHub, e := NewHub(newLogger, robStorage)
	if e != nil {
		newLogger.Fatalf("failed to create grpc hub: %v", e)
	}

	return grpcHub
}
