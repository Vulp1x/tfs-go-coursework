package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/pb"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/robots"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/myerr"
	"google.golang.org/grpc"
)

const (
	hubSleepDuration = 3 * time.Second
	hubBufferSize    = 256
)

type Hub struct {
	client pb.TradingServiceClient

	tickersPrices    map[string]pb.TradingService_PriceClient
	readyRobots      map[string]map[uuid.UUID]*grpcRobot
	registeredRobots map[uuid.UUID]bool

	logger     logger.Logger
	robStorage robots.Storage

	sendToSocket chan *robots.Robot
}

type grpcRobot struct {
	*robots.Robot

	PriceChannel chan *pb.PriceResponse
	hub          *Hub
}

func (h *Hub) newGrpcRobot(r *robots.Robot) *grpcRobot {
	return &grpcRobot{
		Robot:        r,
		PriceChannel: make(chan *pb.PriceResponse),
		hub:          h,
	}
}

func NewHub(logger logger.Logger, storage robots.Storage) (*Hub, error) {
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		return nil, myerr.Wrap(err, "Failed to connect to grpc server")
	}

	client := pb.NewTradingServiceClient(conn)

	return &Hub{
		client:           client,
		tickersPrices:    make(map[string]pb.TradingService_PriceClient),
		readyRobots:      make(map[string]map[uuid.UUID]*grpcRobot),
		registeredRobots: make(map[uuid.UUID]bool),
		logger:           logger,
		robStorage:       storage,
		sendToSocket:     make(chan *robots.Robot, hubBufferSize),
	}, nil
}

func (h *Hub) Run() {
	for {
		rawRobots, err := h.robStorage.FindReadyRobots()
		if err != nil {
			h.logger.Fatalf("Failed to find all ready robots: %v", err)

			return
		}

		h.checkForNewTickers(rawRobots)

		for ticker, robs := range rawRobots {
			price, err := h.tickersPrices[ticker].Recv()
			if err != nil {
				h.logger.Errorf("Failed to get price response for ticker: %s from server: %v", ticker, err)

				return
			}

			for _, rob := range robs {
				// if robot is not registered register it
				if !h.registeredRobots[rob.RobotID] {
					h.registeredRobots[rob.RobotID] = true
					add(h.readyRobots, ticker, rob.RobotID, h.newGrpcRobot(rob))

					go h.readyRobots[rob.Ticker][rob.RobotID].StartTrade()
				}

				h.readyRobots[rob.Ticker][rob.RobotID].PriceChannel <- price
			}
		}

		time.Sleep(hubSleepDuration)
	}
}

// check if new tickers appeared.
func (h *Hub) checkForNewTickers(rawRobots map[string][]*robots.Robot) {
	keys := make([]string, len(rawRobots))

	i := 0

	for k := range rawRobots {
		keys[i] = k
		i++
	}

	for _, ticker := range keys {
		_, ok := h.tickersPrices[ticker]
		if !ok {
			stream, err := h.client.Price(context.Background(), &pb.PriceRequest{Ticker: ticker})
			if err != nil {
				h.logger.Fatalf("Failed to get price stream for ticker: %s", ticker)

				return
			}

			h.tickersPrices[ticker] = stream
		}
	}
}

func (g *grpcRobot) StartTrade() {
	if !g.Robot.IsWorkingNow() {
		fmt.Println("isn't working now")
		g.hub.unregisterRobot(g.Robot)

		return
	}

	timer := time.NewTimer(time.Until(g.PlanEnd.Time))

	for {
		select {
		case price := <-g.PriceChannel:
			g.tryToPerformTrade(price)
		case <-timer.C:
			fmt.Println("unregister robot: ", g.Robot)
			g.hub.unregisterRobot(g.Robot)

			return
		}
	}
}

func (h *Hub) unregisterRobot(r *robots.Robot) {
	delete(h.registeredRobots, r.RobotID)
	delete(h.readyRobots[r.Ticker], r.RobotID)
}

func (h *Hub) SaveRobots() {
	url := "ws://localhost:6000/ws/update"
	header := make(http.Header)
	header.Add("Origin", "http://localhost:5000/grpc")

	conn, resp, err := websocket.DefaultDialer.Dial(url, header)
	if err != nil {
		h.logger.Fatalf("Failed to connect to websocket private endpoint: %v", err)

		return
	}

	defer func() {
		resp.Body.Close()
		_ = conn.Close()
	}()

	for robot := range h.sendToSocket {
		r := robot
		fmt.Println("writing robot to socket: ", r)

		e := conn.WriteJSON(&r)
		if e != nil {
			h.logger.Errorf("failed to write to socket: %v", e)
		}
	}
}

func (h *Hub) saveRobotInDB(robot *robots.Robot) (*robots.Robot, error) {
	if robot == nil {
		return nil, myerr.New("")
	}

	fmt.Println("saving robot")

	return h.robStorage.Update(robot)
}

func (g *grpcRobot) tryToPerformTrade(price *pb.PriceResponse) {
	if g.DealsCount == 0 {
		return
	}

	tradeDone := false

	if g.IsSelling {
		if price.SellPrice > g.SellPrice {
			fmt.Println("selling:")

			g.IsSelling = false

			g.DealsCount--

			g.FactYield += price.SellPrice

			tradeDone = true
		}
	} else if price.BuyPrice < g.BuyPrice {
		fmt.Println("buying")
		g.IsSelling = true
		g.DealsCount--

		g.FactYield -= price.BuyPrice
		tradeDone = true
	}

	if tradeDone {
		fmt.Println("performed trade: ", g.RobotID, g.IsSelling)

		rob, err := g.hub.saveRobotInDB(g.Robot)
		if err != nil {
			g.hub.logger.Errorf("Failed to save robot in db")
		}

		g.hub.sendToSocket <- rob
	}
}

func add(m map[string]map[uuid.UUID]*grpcRobot, ticker string, id uuid.UUID, rob *grpcRobot) {
	mm, ok := m[ticker]
	if !ok {
		mm = make(map[uuid.UUID]*grpcRobot)
		m[ticker] = mm
	}

	mm[id] = rob
}
