package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/middlewares"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/robots"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/myerr"
)

const bufferSize = 1024

var upgrader = websocket.Upgrader{ //nolint
	ReadBufferSize:  bufferSize,
	WriteBufferSize: bufferSize,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan *robots.Robot

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// Update robot in database
	update chan *robots.Robot

	logger       logger.Logger
	robStorage   robots.Storage
	loggerConfig logger.Configuration
}

func NewHub(loggerConfig logger.Configuration, hubLogger logger.Logger, robotStorage robots.Storage) *Hub {
	return &Hub{
		broadcast:    make(chan *robots.Robot),
		register:     make(chan *Client),
		unregister:   make(chan *Client),
		clients:      make(map[*Client]bool),
		update:       make(chan *robots.Robot),
		loggerConfig: loggerConfig,
		logger:       hubLogger,
		robStorage:   robotStorage,
	}
}

func (h *Hub) PrivateRoutes() chi.Router {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Timeout(timeout))
	r.Use(middleware.Recoverer)
	r.Use(middlewares.AddLoggerConfig(h.loggerConfig))
	r.Use(middleware.RequestLogger(&middlewares.StructuredLogger{Logger: h.logger}))

	r.Route("/ws", func(r chi.Router) {
		r.Get("/update", h.serveGrpcUpdates)
	})

	return r
}

func (h *Hub) Routes() chi.Router {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Timeout(timeout))
	r.Use(middleware.Recoverer)
	r.Use(middlewares.AddLoggerConfig(h.loggerConfig))
	r.Use(middleware.RequestLogger(&middlewares.StructuredLogger{Logger: h.logger}))

	r.Route("/ws", func(r chi.Router) {
		r.Get("/robot/{robotID}", h.serveRobotWs)
		r.Get("/robots", h.serveCatalogWs)
	})

	return r
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case robot := <-h.broadcast:
			h.sendUpdate(robot)

		case robot := <-h.update:
			h.sendUpdate(robot)
		}
	}
}

func (h *Hub) SendToBroadcast(rob *robots.Robot) error {
	if rob == nil {
		return myerr.New("Nil robot provided")
	}

	h.broadcast <- rob

	return nil
}

func (h *Hub) Logger() logger.Logger {
	return h.logger
}

// serveRobotWs handles websocket requests for one robot updates.
func (h *Hub) serveRobotWs(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		reqLogger.Errorf("Failed to upgrade websocket: %v", err)

		return
	}

	robotID, err := uuid.Parse(chi.URLParam(r, "robotID"))
	if err != nil {
		reqLogger.Warnf("Failed to parse robotID from url")
		http.Error(w, "Bad robot id provided", http.StatusBadRequest)

		return
	}

	client := newRobotClient(h, conn, robotID)
	client.hub.register <- client

	h.clients[client] = true

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writeToSocket()
	go client.handlePingPong()
}

// serveCatalogWs handles websocket request for updates on robots catalog.
func (h *Hub) serveCatalogWs(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		reqLogger.Errorf("Failed to upgrade websocket: %v", err)

		return
	}

	values := r.URL.Query()

	rob, er := robots.FilterIDS(values.Get("userID"), values.Get("ticker"), h.robStorage)
	if er != nil {
		middlewares.InternalError(w, r, "Failed to get robots ID from the database")

		return
	}

	client := newCatalogueClient(h, conn, rob)
	userIDString := values.Get("userID")

	if userIDString == "" {
		client.userID = uuid.Nil
	} else {
		client.userID, _ = uuid.Parse(userIDString)
	}

	client.ticker = values.Get("robotID")

	h.register <- client

	h.clients[client] = true

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writeToSocket()
	go client.handlePingPong()
}

// serveGrpcUpdates handle robot updates after trades are made.
func (h *Hub) serveGrpcUpdates(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger

	privateUpgrader := websocket.Upgrader{
		ReadBufferSize:  bufferSize,
		WriteBufferSize: bufferSize,
		CheckOrigin: func(r *http.Request) bool {
			origin := r.Header.Get("Origin")

			// only clients of grpc server could connect to this webSocket
			return origin == "http://localhost:5000/grpc"
		},
	}

	conn, err := privateUpgrader.Upgrade(w, r, nil)
	if err != nil {
		reqLogger.Errorf("Failed to upgrade websocket: %v", err)

		return
	}

	var rob robots.Robot

	for {
		err := conn.ReadJSON(&rob)
		if err != nil {
			if err == io.EOF {
				break
			}

			if websocket.IsCloseError(err, websocket.CloseAbnormalClosure) {
				break
			}

			h.logger.Warnf("can't receive from private socket: %s", err)

			break
		}

		h.broadcast <- &rob
	}
}

func (h *Hub) sendUpdate(robot *robots.Robot) {
	for client := range h.clients {
		fmt.Println()

		if client.isAccessGranted(robot.RobotID) {
			select {
			case client.send <- robot:
			default:
				close(client.send)
				delete(h.clients, client)
			}
		}
	}

	fmt.Println("im done")
}

func (h *Hub) UpdateAcceptedIDs() {
	for client, ok := range h.clients {
		if !ok {
			h.unregister <- client
		}

		ids, err := h.robStorage.FilterIDsByTickerAndUser(client.ticker, client.userID)
		if err != nil {
			h.logger.Errorf("failed to update accepted ids for client %d, %s", client.userID, client.ticker)

			return
		}

		m := make(map[uuid.UUID]bool)
		for _, robotID := range ids {
			m[robotID] = true
		}

		client.acceptedIDs = m
	}
}
