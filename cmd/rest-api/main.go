package main

import (
	"fmt"
	"log"
	"net/http"
	"reflect"
	"runtime"

	"github.com/go-chi/chi"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/config"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/postgres"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
)

func main() {
	handler := initHandler()
	wsHub := handler.socketHub
	newLogger := handler.logger
	r := handler.Routes()

	printRouterDocs(r)
	fmt.Println("prepared: ")

	r.Mount("/", wsHub.Routes())

	go wsHub.Run()

	go func() {
		fmt.Println("private server started")

		if err := http.ListenAndServe(":6000", wsHub.PrivateRoutes()); err != nil {
			newLogger.Fatalf("Got error from private API server: %v", err)
		}
	}()

	if err := http.ListenAndServe(":8000", r); err != nil {
		newLogger.Fatalf("Got Error from main Server: %v", err)
	}
}

func initHandler() *Handler {
	conf := &config.Config{}

	err := conf.ParseConfiguration()
	if err != nil {
		log.Fatal("Failed to parse configuration: ", err)
	}

	newLogger, err := logger.NewLogger(conf.Logger)
	if err != nil {
		log.Panic("Could not instantiate log: ", err)
	}

	db, err := postgres.NewDB(conf.Postgres)
	if err != nil {
		log.Panic("Failed to connect to database: ", err)
	}

	userStorage, err := postgres.NewUserStorage(db)
	if err != nil {
		newLogger.Fatalf("failed to create new userStorage: %s", err)
	}

	robStorage, err := postgres.NewRobotStorage(db)
	if err != nil {
		newLogger.Fatalf("Failed to create new Robot storage: %s", err)
	}

	wsHub := NewHub(conf.Logger, newLogger, robStorage)

	return NewHandler(newLogger, userStorage, robStorage, wsHub, conf.Security, conf.Logger)
}

func printRouterDocs(r chi.Router) {
	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		fmt.Printf("listen %s %s\n", method, route)

		for _, middleware := range middlewares {
			typ := runtime.FuncForPC(reflect.ValueOf(middleware).Pointer())
			fmt.Println(typ.Name())
		}

		return nil
	}

	if err := chi.Walk(r, walkFunc); err != nil {
		fmt.Printf("Logging err: %s\n", err.Error())
	}
}
