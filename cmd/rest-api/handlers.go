package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/google/uuid"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/middlewares"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/postgres"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/robots"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/sessions"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/users"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/logger"
	"gitlab.com/Vulp1x/tfs-go-coursework/pkg/myerr"
)

const timeout = 40 * time.Second

type Handler struct {
	logger        logger.Logger
	userStorage   users.Storage
	robotStorage  robots.Storage
	socketHub     *Hub
	sessionConfig sessions.Configuration
	loggerConfig  logger.Configuration
}

func NewHandler(l logger.Logger, us users.Storage, rs robots.Storage, h *Hub,
	sesConf sessions.Configuration, logConf logger.Configuration) *Handler {
	return &Handler{
		logger:        l,
		userStorage:   us,
		robotStorage:  rs,
		socketHub:     h,
		sessionConfig: sesConf,
		loggerConfig:  logConf,
	}
}

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Timeout(timeout))
	r.Use(middleware.Recoverer)
	r.Use(middleware.SetHeader("Content-type", "application/json"))
	r.Use(middlewares.AddLoggerConfig(h.loggerConfig))
	r.Use(middleware.RequestLogger(&middlewares.StructuredLogger{Logger: h.logger}))

	r.Route("/api/v1", func(r chi.Router) {
		r.Route("/", func(r chi.Router) {
			r.Use(middlewares.ReadBody())
			r.Post("/signup", h.CreateUser)
			r.With(middlewares.CheckCredentials(h.userStorage, h.sessionConfig)).Route("/", func(r chi.Router) {
				r.Post("/signin", h.SignIn)
			})
		})

		r.Route("/robots", func(r chi.Router) {
			r.Use(middlewares.ReadBody())
			r.Use(middlewares.CheckSession(h.sessionConfig))
			r.Get("/", h.FilterRobots)
		})

		r.Route("/user", func(r chi.Router) {
			r.With(middlewares.AddIDFromURL(middlewares.UserID)).Route("/{userID}", func(r chi.Router) {
				r.Use(middlewares.CheckSession(h.sessionConfig))
				r.Use(middlewares.CheckAccessToURL(h.userStorage))
				r.Get("/", h.GetUser)

				r.With(middlewares.ReadBody()).Route("/", func(r chi.Router) {
					r.Put("/", h.UpdateUser)
					r.Get("/robots", h.GetUserRobots)
				})
			})
		})

		r.Route("/robot", func(r chi.Router) {
			r.Use(middlewares.ReadBody())
			r.Use(middlewares.CheckSession(h.sessionConfig))
			r.Post("/", h.CreateRobot)
			r.With(middlewares.AddIDFromURL(middlewares.RobotID)).Route("/{robotID}", func(r chi.Router) {
				r.Get("/", h.GetRobot)
				r.Put("/", h.UpdateRobot)
				r.Delete("/", h.DeleteRobot)
				r.Put("/activate", h.ActivateRobot)
				r.Put("/deactivate", h.DeactivateRobot)
				r.Put("/favourite", h.MakeRobotFavorite) //nolint:misspell
			})
		})
	})

	return r
}

func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger

	body := middlewares.GetBody(r)

	var u users.User

	err := u.CreateNewUser(body)
	if err != nil {
		fmt.Println(string(body))
		middlewares.Error(w, r, http.StatusBadRequest, "failed to unmarshal User json: %v", err)

		return
	}

	fmt.Println(u.ID.String())

	if err := h.userStorage.Create(&u); err != nil {
		if errors.Is(err, postgres.ErrEmailIsAlreadyUsed) {
			middlewares.Error(w, r, http.StatusConflict, "Email %s isn't unique", u.Email)

			return
		}

		middlewares.InternalError(w, r, "got error from DB Create: %v", err)

		return
	}

	w.WriteHeader(http.StatusCreated)
	reqLogger.Infof("Created new User: %s, ", u)
}

func (h *Handler) SignIn(w http.ResponseWriter, r *http.Request) {
	signedToken := middlewares.GetToken(r)
	ses := middlewares.GetClaims(r)

	w.Header().Add("Bearer", signedToken)

	_, err := w.Write([]byte(fmt.Sprintf("UserID: %s", ses.UserID)))
	if err != nil {
		middlewares.InternalError(w, r, "Failed to write answer: %v", err)
	}
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger
	user := middlewares.GetUser(r)

	var bodyUser users.User

	err := json.Unmarshal(middlewares.GetBody(r), &bodyUser)
	if err != nil {
		middlewares.InternalError(w, r, "failed to parse user from JSON: %v", err)

		return
	}

	err = user.UpdateUser(bodyUser)
	if err != nil {
		middlewares.InternalError(w, r, "User wasn't updated: %s", err)

		return
	}

	err = h.userStorage.Update(user)
	if err != nil {
		if errors.Is(err, postgres.ErrEmailIsAlreadyUsed) {
			reqLogger.Infof("Couldn't update user, email should be unique: %s", user.Email)
			middlewares.Error(w, r, http.StatusBadRequest, "Another user already has this email: "+user.Email)

			return
		}

		middlewares.InternalError(w, r, "Failed to update user in db: %s", err)

		return
	}

	err = sendJSON(w, user, nil)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %s", err)
	}
}

func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	existingUser := middlewares.GetUser(r)

	err := sendJSON(w, existingUser, nil)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %s", err)
	}
}

func (h *Handler) CreateRobot(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger
	ses := middlewares.GetClaims(r)
	body := middlewares.GetBody(r)

	rob := &robots.Robot{}

	err := rob.CreateNewRobot(body, ses.UserID, h.robotStorage)
	if err != nil {
		fmt.Println(err)
		handleRobotErrors("create", err, w, r)

		return
	}

	reqLogger.Infof("New robot created: %s", rob)

	_, err = w.Write([]byte(fmt.Sprintf("RobotID: %s", rob.RobotID)))
	if err != nil {
		middlewares.InternalError(w, r, "Failed to write answer: %v", err)
	}
}

func (h Handler) DeleteRobot(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger
	ses := middlewares.GetClaims(r)

	robotID := middlewares.GetRobotID(r)

	err := robots.DeleteRobot(robotID, ses.UserID, h.robotStorage)
	if err != nil {
		handleRobotErrors("delete", err, w, r)

		return
	}

	reqLogger.Infof("Robot %d deleted", robotID)
}

func (h *Handler) GetUserRobots(w http.ResponseWriter, r *http.Request) {
	dbUser := middlewares.GetUser(r)

	robotsList, err := h.robotStorage.FilterRobotsByUser(dbUser.ID)
	if err != nil {
		handleRobotErrors("get", err, w, r)

		return
	}

	err = sendJSON(w, robotsList, nil)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %s", err)
	}
}

func (h *Handler) FilterRobots(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()

	robs, err := robots.FilterRobots(values.Get("user"), values.Get("ticker"), h.robotStorage)
	if err != nil {
		handleRobotErrors("filter by user and ticker", err, w, r)

		return
	}

	err = sendJSON(w, robs, nil)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %s", err)
	}
}

func (h *Handler) MakeRobotFavorite(w http.ResponseWriter, r *http.Request) {
	ses := middlewares.GetClaims(r)
	reqLogger := middlewares.GetLogEntry(r).Logger

	robotID, err := uuid.Parse(chi.URLParam(r, "robotID"))
	if err != nil {
		reqLogger.Warnf("Failed to parse robotID from url: %s", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	rob, er := robots.MakeFavorite(ses.UserID, robotID, h.robotStorage)
	if er != nil {
		handleRobotErrors("make favourite", er, w, r) //nolint:misspell

		return
	}

	err = sendJSON(w, rob, h.socketHub)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %v", err)
	}
}

func (h *Handler) ActivateRobot(w http.ResponseWriter, r *http.Request) {
	ses := middlewares.GetClaims(r)
	robotID := middlewares.GetRobotID(r)

	rob, e := robots.Activate(robotID, ses.UserID, h.robotStorage)
	if e != nil {
		handleRobotErrors("activate", e, w, r)

		return
	}

	fmt.Println(h.socketHub)

	err := sendJSON(w, rob, h.socketHub)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %v", err)
	}
}

func (h *Handler) DeactivateRobot(w http.ResponseWriter, r *http.Request) {
	ses := middlewares.GetClaims(r)
	reqLogger := middlewares.GetLogEntry(r).Logger

	robotID, err := uuid.Parse(chi.URLParam(r, "robotID"))
	if err != nil {
		reqLogger.Infof("Failed to parse robotID from url: %v", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	rob, err := robots.Deactivate(robotID, ses.UserID, h.robotStorage)
	if err != nil {
		handleRobotErrors("deactivate", err, w, r)

		return
	}

	err = sendJSON(w, rob, h.socketHub)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %v", err)
	}
}

func (h *Handler) UpdateRobot(w http.ResponseWriter, r *http.Request) {
	ses := middlewares.GetClaims(r)
	reqLogger := middlewares.GetLogEntry(r).Logger
	robotID := middlewares.GetRobotID(r)
	body := middlewares.GetBody(r)

	rob, err := robots.Update(body, ses.UserID, robotID, h.robotStorage)
	if err != nil {
		handleRobotErrors("update", err, w, r)

		return
	}

	e := sendJSON(w, rob, h.socketHub)
	if e != nil {
		middlewares.InternalError(w, r, "Failed to send response: %v", e)

		return
	}

	reqLogger.Infof("Updated robot and sent it to socket")
}

func (h *Handler) GetRobot(w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger
	ses := middlewares.GetClaims(r)
	robotID := middlewares.GetRobotID(r)

	rob, err := h.robotStorage.FindByID(robotID)
	if err != nil {
		middlewares.Error(w, r, http.StatusNotFound, "Failed to get robot with id %d: %v", robotID, err)

		return
	}

	if ses.UserID != rob.OwnerUserID {
		reqLogger.Warnf("List of robots for user %d is prohibited for user %d", robotID, ses.UserID)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return
	}

	err = sendJSON(w, rob, nil)
	if err != nil {
		middlewares.InternalError(w, r, "Failed to send response: %v", err)
	}
}

// Marshal body to JSON then send it as response and as a websocket message
// if socketHub parameter is nil, then no messages are sent to websocket.
func sendJSON(w http.ResponseWriter, body interface{}, socketHub *Hub) error { //nolint
	fmt.Printf("body: %+v\n", body)

	payload, err := json.Marshal(body)
	if err != nil {
		return err
	}

	_, e := w.Write(payload)
	if e != nil {
		return e
	}

	var sendToBroadcastError error

	if socketHub != nil {
		go func() {
			// socketHub.UpdateAcceptedIDs()
			rob, ok := body.(*robots.Robot)
			if !ok {
				socketHub.Logger().Warnf("Failed to cast websocket message type: %s to robot type",
					reflect.TypeOf(body))

				return
			}

			fmt.Println("SEND: ", string(payload))

			err := socketHub.SendToBroadcast(rob)
			if err != nil {
				sendToBroadcastError = err
			}
		}()
	}

	return sendToBroadcastError
}

// method is a name of operation, that was performed on Robot instance.
// Could be one of the following: create, make favorite, activate, deactivate, delete.
func handleRobotErrors(method string, err error, w http.ResponseWriter, r *http.Request) {
	reqLogger := middlewares.GetLogEntry(r).Logger

	if err == nil {
		reqLogger.Infof("No error provided, returning")

		return
	}

	e, ok := err.(myerr.ErrorInterface)
	if !ok {
		middlewares.InternalError(w, r, "Failed to cast error to ErrorInterface, got type: %s. Error: %v",
			reflect.TypeOf(err), err)

		return
	}

	if done := handleBadInputRobotErrors(e, w, r); done {
		return
	}

	switch {
	case e.Is(robots.ErrAlreadyWorking):
		reqLogger.Infof("Robot %d is already working, couldn't "+method+" it",
			middlewares.GetRobotID(r))
		http.Error(w, "Robot is working already, couldn't activate it",
			http.StatusConflict)

	case e.Is(robots.ErrAlreadyActive):
		reqLogger.Infof("Robot is already active")
		http.Error(w, "Robot is already activated", http.StatusBadRequest)

	case e.Is(robots.ErrAlreadyInactive):
		reqLogger.Infof("Robot is already deactivated")
		http.Error(w, "Robot is already deactivated", http.StatusBadRequest)

	case e.Is(robots.ErrOperationFailed):
		middlewares.InternalError(w, r, "Failed to "+method+" robot %d: %s", middlewares.GetRobotID(r), e.Unwrap())

	default:
		middlewares.InternalError(w, r, "Got unknown error: %v", e)
	}
}

func handleBadInputRobotErrors(e myerr.ErrorInterface, w http.ResponseWriter, r *http.Request) bool {
	ses := middlewares.GetClaims(r)
	reqLogger := middlewares.GetLogEntry(r).Logger

	switch {
	case e.Is(robots.ErrBadJSON):
		reqLogger.Infof("Bad robot JSON provided: %v", e)
		http.Error(w, "Bad body JSON provided", http.StatusBadRequest)

	case e.Is(robots.ErrInvalidRobotID):
		reqLogger.Infof("There is no robot with id: %d", middlewares.GetRobotID(r))
		http.NotFound(w, r)

	case e.Is(robots.ErrForbidden):
		reqLogger.Infof("Robot %d is forbidden for user %d", middlewares.GetRobotID(r), ses.UserID)
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)

	case e.Is(robots.ErrScanFailed):
		middlewares.InternalError(w, r, "Scan failed: %s", e.Unwrap())

	case e.Is(robots.ErrNotFound):
		reqLogger.Errorf("Failed to find robots by criteria: %v", e.Unwrap())
		http.NotFound(w, r)

	case e.Is(robots.ErrBadQueryParams):
		reqLogger.Infof("%s", e)
		http.Error(w, "Bad query parameters", http.StatusBadRequest)

	default:
		return false
	}

	return true
}
