package main

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/Vulp1x/tfs-go-coursework/internal/robots"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512

	channelCapacity = 256
)

type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *robots.Robot

	acceptedIDs map[uuid.UUID]bool

	ticker string
	userID uuid.UUID
}

// isAccessGranted checks if client need updates for Robot with robot_id=robotID.
func (c *Client) isAccessGranted(robotID uuid.UUID) bool {
	access, _ := c.acceptedIDs[robotID] //nolint

	return access
}

// handlePingPong pumps messages from the websocket connection to the hub.
//
// The application runs handlePingPong in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) handlePingPong() {
	defer func() {
		c.hub.unregister <- c
		_ = c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))

		return nil
	})

	for {
		_, _, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				c.hub.logger.Errorf("error: %v", err)
			}

			break
		}
	}
}

// writeToSocket pumps messages from the hub to the websocket connection.
//Client
// A goroutine running writeToSocket is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writeToSocket() {
	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()

		_ = c.conn.Close()
	}()

	for {
		select {
		case robot, ok := <-c.send:
			fmt.Printf("Sending robot: %s\n", robot)

			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))

			if !ok {
				// The hub closed the channel.
				_ = c.conn.WriteMessage(websocket.CloseMessage, []byte{})

				return
			}

			err := c.conn.WriteJSON(robot)
			if err != nil {
				c.hub.logger.Warnf("Failed to write robot %s to socket: %v", robot, err)

				return
			}

		case <-ticker.C:
			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))

			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func newRobotClient(hub *Hub, conn *websocket.Conn, robotID uuid.UUID) *Client {
	return &Client{
		hub:         hub,
		conn:        conn,
		send:        make(chan *robots.Robot, channelCapacity),
		acceptedIDs: map[uuid.UUID]bool{robotID: true},
	}
}

func newCatalogueClient(hub *Hub, conn *websocket.Conn, robotsIDs []uuid.UUID) *Client {
	m := make(map[uuid.UUID]bool)
	for _, robotID := range robotsIDs {
		m[robotID] = true
	}

	return &Client{
		hub:         hub,
		conn:        conn,
		send:        make(chan *robots.Robot, channelCapacity),
		acceptedIDs: m,
	}
}
